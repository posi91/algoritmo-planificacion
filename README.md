# Algoritmos de Planificación

Los algoritmos a probar son FcFs, SJF, Round Robin Con Manejo y Round Robin Sin manejo.

## Integrantes: 
* Jonathan Posada 
* Natalia Obando

**Sistemas Operacionales - Politecnico Grancolombiano**

## Tabla de Contenido
- Introduccion
- FCFFS
- Round Robin
- SJF
- Guia - Uso del aplicativo

## Introducción

Aquí vamos a centrarnos en analizar los distintos tipos de algoritmos de planificación.  
Estos algoritmos surgen debido a la necesidad de poder organizar los procesos de una manera 
eficiente para el procesador.

Los algoritmos de planificación se encargan de asegurar que un proceso no monopoliza 
el procesador. Un proceso es un programa que está en ejecución. Este proceso puede estar 
en 3 estados distintos  “Listo” “Bloqueado” y “En Ejecución”. Los procesos son almacenados
en una lista  junto con la información que indica en qué estado está el proceso, 
el tiempo que ha usado el CPU, etc.


## FCFS
Primero en entrar primero en salir. A medida que un proceso pasa al estado listo, este es agregado a la cola de listos.
Cuando el proceso actualmente está ejecutando cesa su ejecución entonces el proceso más
viejo en la cola es seleccionado para correr.

![Alt text](https://www.geeksforgeeks.org/wp-content/uploads/FCFS.png)

## Round Robin
Una interrupción de reloj es generada a intervalos periódicos.Cuando ocurre la 
interrupción, el proceso en la ejecución es seleccionado basado en el esquema FCFS. 
A cada proceso se le da un trozo de tiempo.

![Alt text](https://cdncontribute.geeksforgeeks.org/wp-content/uploads/round-robin-1.jpg)

## SJF "Shortest Job First"
El algoritmo SJF (Shortest-Job-First) se basa en los ciclos de vida de los procesos, los cuales transcurren en dos etapas o períodos que son: ciclos de CPU y ciclos de entrada/salida, tambien conocidos por rafagas.
La palabra shortest (el más corto) se refiere al proceso que tenga el el próximo ciclo de CPU más corto. La idea es escoger entre todos los procesos listos el que tenga su próximo ciclo de CPU más pequeño.
El SJF se puede comportar de dos formas:
Con Desalojo: Si se incorpora un nuevo proceso a la cola de listos y este tiene un ciclo de CPU menor que el ciclo de CPU del proceso que se esté ejecutando,entonces dicho proceso es desalojado y el nuevo proceso toma la CPU.
Sin desalojo: Cuando un proceso toma la CPU, ningun otro proceso podrá apropiarse de ella hasta que que el proceso que la posee termine de ejecutarse.

![Alt text](http://3.bp.blogspot.com/-ScaJInD_u18/VirriDBmhTI/AAAAAAAAGxo/7CaXN9ko038/s1600/captura2.jpg)

## Guia - Uso del aplicativo
Al correr el fichero vista.java visualizara una ventana como la siguiente:

![Alt text](https://ticsandsec.files.wordpress.com/2018/10/main_interfaz.png)

Seleccionar el metodo de algoritmo que desea simular:

### Round Robin

Agregar un nuevo proceso (Round Robin) con sus respectivos valores:

![Alt text](https://ticsandsec.files.wordpress.com/2018/10/round_robin.png)

### FCFS

Agregar un nuevo proceso (FCFS) con sus respectivos valores:
![Alt text](https://ticsandsec.files.wordpress.com/2018/10/fcfs.png)
